FROM golang:1.9.2 AS builder
WORKDIR /
ADD main.go /
RUN go get -d -v .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o eos2s3-server .
CMD ["/eos2s3-server"]

FROM alpine:latest
WORKDIR /
RUN apk --no-cache add ca-certificates
COPY --from=builder /eos2s3-server /
ADD files /
CMD ["/eos2s3-server"]
