#!/bin/sh

mkdir /root/.aws

printf "host_base = s3.cern.ch\nhost_bucket = %%(bucket)s.s3.cern.ch\naccess_key = $AWS_KEY\nsecret_key = $AWS_SECRET" > /root/.s3cfg
printf "[default]\nregion = nova" > /root/.aws/config
printf "[default]\naws_access_key_id = $AWS_KEY\naws_secret_access_key = $AWS_SECRET" > /root/.aws/credentials



# In the google portal, Cloud Storage/Settings. Check for 'Access keys for your user account', and create one.
printf "[Credentials]\ngs_access_key_id = $GS_ACCESS_KEY_ID\ngs_secret_access_key = $GS_SECRET_ACCESS_KEY\n[Boto]\nhttps_validate_certificates = True\n[GSUtil]\ncontent_language = en\ndefault_api_version = 2\ndefault_project_id = $GS_PROJECT" > /root/.boto

while true; do
  F=$(curl -s $MASTER_URL)
  eos2s3 ${STORETYPE:-gs} $F ${BUCKET:-higgs-demo} ${TMPDIR:-/tmp/$BUCKET}
done
