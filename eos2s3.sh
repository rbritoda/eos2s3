#!/bin/sh
set -x

BUCKET="${3:-higgs-demo}"
TMPDIR="${4:-/tmp/higgs-demo}"

STORETYPE="${1:-gs}"
XRDPATH=$2
XRDFILE=$(echo ${XRDPATH} | sed 's#root://.*cern.ch//##')
S3PATH="${STORETYPE}://${BUCKET}/${XRDFILE}"
TMPPATH="${TMPDIR}/${XRDFILE}"

if [ "${STORETYPE}" == "s3" ]; then
  if s3cmd info $S3PATH > /dev/null 2>&1; then
    echo "WARN: S3 file exists, not touching: ${S3PATH}"
    exit 0
  fi
fi

if [ "${STORETYPE}" == "gs" ]; then
  if gsutil stat $S3PATH > /dev/null 2>&1; then
    echo "WARN: GS file exists, not touching: ${S3PATH}"
    exit 0
  fi
fi

echo "Copying ${XRDPATH} to tmp storage in ${TMPPATH}"
if [ -f $TMPPATH ]; then
  echo "WARN: Temporary file exists, using it: ${TMPPATH}"
else
  mkdir -p $(dirname $TMPPATH)
  echo "xrdcp $XRDPATH $TMPPATH"
  xrdcp $XRDPATH $TMPPATH
fi

echo "Copying to remote storage: ${S3PATH}"
if [ "${STORETYPE}" == "s3" ]; then
  if s3cmd info $S3PATH > /dev/null 2>&1; then
    echo "WARN: S3 file exists, not touching: ${S3PATH}"
  else
    #s3cmd --no-progress put -P $TMPPATH $S3PATH
    aws --endpoint-url https://s3.cern.ch s3 cp $TMPPATH $S3PATH
  fi
elif [ "${STORETYPE}" == "gs" ]; then
  if gsutil stat $S3PATH > /dev/null 2>&1; then
    echo "WARN: GS file exists, not touching: ${S3PATH}"
  else
    gsutil cp $TMPPATH $S3PATH
  fi
fi

echo "Deleting from tmp storage ${TMPPATH}"
rm $TMPPATH
