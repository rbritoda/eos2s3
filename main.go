package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
)

var files chan string
var counter int

func main() {
	counter = 0

	files = make(chan string, 1000000)
	f, err := os.Open("files")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		files <- scanner.Text()
	}

	fmt.Printf("listening on port 7777\n")
	http.HandleFunc("/", handler)
	panic(http.ListenAndServe("0.0.0.0:7777", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	resp := <- files
	counter += 1
	fmt.Printf("%v : %v : %v : %v\n", counter, r.RemoteAddr, r.RequestURI, resp)
	w.Write([]byte(resp))
}
